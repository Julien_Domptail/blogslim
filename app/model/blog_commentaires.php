<?php

use Slim\Slim;

class blog_commentaires extends \Illuminate\Database\Eloquent\Model {

   	protected $table = 'blog_commentaires';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}

?>
