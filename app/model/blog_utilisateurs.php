<?php

use Slim\Slim;

class blog_utilisateurs extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'blog_utilisateurs';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}

?>
