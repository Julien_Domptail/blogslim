<?php

use Slim\Slim;

class blog_categories extends \Illuminate\Database\Eloquent\Model{

  	protected $table = 'blog_categories';
	protected $primaryKey = 'id';
	public $timestamps = false;

}

?>
