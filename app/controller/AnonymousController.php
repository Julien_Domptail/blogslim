<?php
use \app\model\blog_utilisateurs;

class AnonymousController extends Controller {

    public function header() {
	$app = Controller::$app;
	$app->render('header.php',compact('app'));
    }

    public function footer() {
	Controller::$app->render('footer.php');
    }

    public function index(){
	$this->header();
	Controller::$app->render('homepage.php');
	$this->footer();
    }

    public function yopla(){
	$this->header();
	Controller::$app->render('arf.php');
	$this->footer();
    }

    public function affiche_item($id){
	$this->header();
	Controller::$app->render('aff_item.php', compact('id'));
	$this->footer();
    }

    public function insert_info(){
	$app = Controller::$app;
	$nom = $app->request->post('nom');
	$app->flash('info', "J'ai ajouté le nom « $nom »");
	$app->redirectTo('root');
    }
	
	public function affichageFormulaireInscription(){
		$this->header();
		Controller::$app->render('formulaireInscription.php');
		$this->footer();
	}
	
	public function inscription(){
	$app = Controller::$app;
	$pseudo = $app->request->post('pseudo');
	$nom = $app->request->post('nom');
	$prenom = $app->request->post('prenom');
	$membre = new blog_utilisateurs();
	$membre->id =0;
	$membre->pseudo=$app->request->post('pseudo');
	$membre->prenom=$app->request->post('prenom');
	$membre->nom=$app->request->post('nom');
	$membre->mail=$app->request->post('mail');
	$membre->mdp=$app->request->post('motdepasse');
	$membre->profil="membre";
	$membre->radie=0;
	$membre.save();
	$app->flash('info', "$nom $prenom $pseudo");
	$app->redirectTo('root');
    }
}

?>
