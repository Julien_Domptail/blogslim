<?php

use Slim\Slim;
require_once 'vendor/autoload.php';

class Controller {

    // Initialization impossible here: Slim does not yet exist!
    protected static $app;

    public function __construct(){
	if (empty(Controller::$app))
	    Controller::$app = Slim::getInstance();
    }
}

?>
